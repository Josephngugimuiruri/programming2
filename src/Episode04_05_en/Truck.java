package Episode04_05_en;

public class Truck extends Vehicle {
    private static int rentalPrice;

    public static int getRentalPrice() {
        return rentalPrice;
    }

    public static void setRentalPrice(int rentalPrice) {
        Truck.rentalPrice = rentalPrice;
    }

    private double weight;

    public double getWeight() {
        return weight;
    }

    public Truck(String regNo, int prodYear, double consumption,
                 double weight) {
        super(regNo, prodYear, consumption);

        this.weight = weight;
    }

    public String toString() {
        return String.format("%s weight: %f", super.toString(), weight);
    }
}
