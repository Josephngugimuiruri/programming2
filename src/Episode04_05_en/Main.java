package Episode04_05_en;

import java.util.Scanner;

public class Main {
    private static final int VEHICLE_COUNT = 10;

    public static void main() {
//        Test.testInstancePolymorphism();
//        Test.testClassPolymorphism();

        Scanner scanner = new Scanner(System.in);
        RentalCompany company = new RentalCompany();

        setParameters(scanner);

        registerVehicles(scanner, company);

        listVehicles(company);

        executeTransport(scanner, company);
    }

    private static void setParameters(Scanner scanner) {
        System.out.print("Current fuel price: ");
        Vehicle.setFuelPrice(scanner.nextInt());

        System.out.print("Basic rental price: ");
        Vehicle.setRentalPrice(scanner.nextInt());

        System.out.print("Bus rental surcharge: ");
        Bus.setRentalPrice(scanner.nextInt());

        System.out.print("Truck rental surcharge: ");
        Truck.setRentalPrice(scanner.nextInt());
    }

    private static void registerVehicles(Scanner scanner, RentalCompany company) {
        String choice;
        do {
            choice = "";
            System.out.println("What you want to register (N-nothing, B-Bus, T-Truk): ");
            while(choice.isEmpty()) {
                choice = scanner.nextLine();
            }
            switch (choice) {
                case "B":
                    registerBus(scanner, company);
                    break;
                case "T":
                    //company.regiserTruck();
                    break;
            }
        } while(choice.compareTo("N") != 0);
    }

    private static void registerBus(Scanner scanner, RentalCompany company) {
        System.out.print("Reg. No: ");
        String regNo = scanner.nextLine();
        System.out.print("Prod. year: ");
        int prodYear = scanner.nextInt();
        System.out.print("Consumption: ");
        double cons = scanner.nextDouble();
        System.out.print("Max. carried persons: ");
        int persons = scanner.nextInt();

        company.registerVehicle(new Bus(regNo, prodYear, cons, persons));
    }

    private static void listVehicles(RentalCompany company) {
        for(Vehicle v: company.getVehicles()) {
            System.out.println(v.toString());
        }
    }

    private static void executeTransport(Scanner scanner, RentalCompany company) {
        String choice;
        do {
            choice = "";
            System.out.println("What do you want to transport (N-nothing, P-People, M-Material): ");
            while(choice.isEmpty()) {
                choice = scanner.nextLine();
            }
            switch (choice) {
                case "P":
                    executeBusTransport(scanner, company);
                    break;
                case "M":
                    //executeTruckTransport(scanner, company);
                    break;
            }
        } while(choice.compareTo("N") != 0);
    }

    private static void executeBusTransport(Scanner scanner, RentalCompany company) {
        System.out.println("How many people you want to carry?");
        int people = scanner.nextInt();

        Bus transporter = company.findBus(people);

        System.out.println("How long was the transport?");
        int transportDistance = scanner.nextInt();

        System.out.format("Travel cost: %f", transporter.travel(transportDistance));
    }
}
