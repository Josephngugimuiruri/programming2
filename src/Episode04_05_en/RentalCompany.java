package Episode04_05_en;


import java.util.ArrayList;

public class RentalCompany {
    private ArrayList<Vehicle> vehicles;

    public ArrayList<Vehicle> getVehicles() {
        return (ArrayList<Vehicle>)vehicles.clone();
    }

    public RentalCompany() {
        this.vehicles = new ArrayList<>();
    }

    public void registerVehicle(Vehicle newVehicle) {
        vehicles.add(newVehicle);
    }

    public void removeVehicle(Vehicle toRemove) {
        vehicles.remove(toRemove);
    }

    public Bus findBus(int persons) {
        for(Vehicle v: vehicles) {
            if((v instanceof Bus) && ((Bus)v).getPersons() >= persons) {
                return (Bus)v;
            }
        }
        return  null;
    }

    public Truck findTruck(double weight) {
        for(Vehicle v: vehicles) {
            if((v instanceof Truck) && ((Truck)v).getWeight() >= weight) {
                return (Truck)v;
            }
        }
        return  null;
    }
}
