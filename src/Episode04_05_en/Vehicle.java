package Episode04_05_en;

public class Vehicle {
    private static int fuelPrice;
    private static int rentalPrice;

    public static void setFuelPrice(int fuelPrice) {
        Vehicle.fuelPrice = fuelPrice;
    }

    public static void setRentalPrice(int rentalPrice) {
        Vehicle.rentalPrice = rentalPrice;
    }

    public static int getRentalPrice() {
        return rentalPrice;
    }

    private String regNo;
    private int prodYear;
    private double consumption;
    private int distanceTravelled;

    public String getRegNo() {
        return regNo;
    }

    public int getProdYear() {
        return prodYear;
    }

    public double getConsumption() {
        return consumption;
    }

    public int getDistanceTravelled() {
        return distanceTravelled;
    }

    public double travel(int distance){
        if(distance > 0) {
            distanceTravelled += distance;
        }

        return (distance/100.0) * consumption * fuelPrice;
    }

    public Vehicle(String regNo, int prodYear, double consumption) {
        this.regNo = regNo;
        this.prodYear = prodYear;
        this.consumption = consumption;
        distanceTravelled = 0;
    }

    @Override
    public String toString() {
        return String.format("%s (%d)", regNo, prodYear);
    }
}
