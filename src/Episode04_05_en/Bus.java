package Episode04_05_en;

public class Bus extends Vehicle {
    private static int rentalPrice;

    public static int getRentalPrice() {
        return rentalPrice;
    }

    public static void setRentalPrice(int rentalPrice) {
        Bus.rentalPrice = rentalPrice;
    }

    private int persons;

    public int getPersons() {
        return persons;
    }

    public Bus(String regNo, int prodYear, double consumption,
               int persons) {
        super(regNo, prodYear, consumption);

        this.persons = persons;
    }

    public String toString() {
        return String.format("%s persons: %d", super.toString(), persons);
    }
}
