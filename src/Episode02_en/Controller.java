package Episode02_en;

public class Controller {
    static final int NUMBER_OF_PERSONS = 5;

    static public void main() {
        exercise01();
    }

    private static void exercise01() {
        PersonArray personArray = new PersonArray(NUMBER_OF_PERSONS);

        personArray.printArray();
        personArray.sortArray();
        personArray.printArray();

        personArray.get(2).setHeight(198);
    }

}
