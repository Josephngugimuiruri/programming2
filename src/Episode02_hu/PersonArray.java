package Episode02_hu;

import java.util.Arrays;
import java.util.Scanner;

public class PersonArray {

    private Person[] persons;

    public Person[] getPersons() {
        return persons;
    }

    public void setPersons(Person[] persons) {
        this.persons = persons;
    }

    public PersonArray(int numberOfPersons) {
        persons = getPersons(numberOfPersons);
    }

    private Person[] getPersons(int numberOfPersons) {
        Person[] persons = new Person[numberOfPersons];

        Scanner scanner = new Scanner(System.in);
        for(int i=0;i<persons.length; i++) {
            persons[i] = new Person();
            persons[i].setWeight(scanner.nextInt());
            persons[i].setHeight(scanner.nextInt());
        }

        return persons;
    }

    public void printArray(Person[] persons) {
        for (int i=0;i<persons.length;i++) {
            System.out.println((i+1)+". person -"
                    + " weight: " + persons[i].getWeight()
                    + " height: " + persons[i].getHeight()
            );
        }
    }

    public void sortArray(Person[] persons) {
        boolean hasSwap;
        do {
            hasSwap = false;
            for (int i = 0; i < persons.length - 1; i++) {
                if (persons[i].getHeight() > persons[i + 1].getHeight()) {
                    Person temp = persons[i];
                    persons[i] = persons[i + 1];
                    persons[i + 1] = temp;

                    hasSwap = true;
                }
            }
        } while (hasSwap);
    }
}
