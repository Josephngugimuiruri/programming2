package Episode04_polymorphism;

import Episode04_polymorphism.Parent;

public class Child extends Parent {

    public static String getStaticClassName()
    {
        return "StaticChild";
    }

    public String getClassName(String a)
    {
        return "Child";
    }

}
