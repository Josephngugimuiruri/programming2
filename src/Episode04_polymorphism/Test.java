package Episode04_polymorphism;

public class Test {
    public static void testInstancePolymorphism() {
        Parent parent = new Parent();
        Child child = new Child();
        Parent parent2 = child;
        Child child2 = (Child)parent2;

        System.out.println(parent.getClassName("a"));

        System.out.println(child.getClassName("a"));
        System.out.println(parent2.getClassName("a"));
        System.out.println(child2.getClassName("a"));
    }

    public static void testClassPolymorphism() {
        Parent parent = new Parent();
        Child child = new Child();
        Parent parent2 = child;
        Child child2 = (Child)parent2;

        System.out.println(Parent.getStaticClassName());
        System.out.println(Child.getStaticClassName());

        System.out.println(child.getStaticClassName());
        System.out.println(parent2.getStaticClassName());
        System.out.println(child2.getStaticClassName());
    }
}
