package Episode04_polymorphism;

public class Parent {

    public static String getStaticClassName()
    {
        return "StaticParent";
    }

    protected String getClassName(String argument)
    {
        return "Parent";
    }

}
