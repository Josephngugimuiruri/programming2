package Episode04_05_06_hu;

import java.util.Scanner;

public class Main {
    public static void main() {
//        Test.testInstancePolymorphism();
//        Test.testClassPolymorphism();

        Scanner consoleScanner = new Scanner(System.in);

        System.out.print("Telephely név: ");
        String name = consoleScanner.nextLine();

        System.out.print("Telephely cím: ");
        String address = consoleScanner.nextLine();

        System.out.print("Telephely azonosító: ");
        String siteId = consoleScanner.nextLine();

        System.out.print("Maximális járműszám: ");
        int maxVehicles = consoleScanner.nextInt();

        Site mySite = new Site(name, address, siteId, maxVehicles);

        mySite.registerVehicle(
                new Car("ABC123", 2002, 7.0, 5, 4)
        );

        mySite.registerVehicle(
                new Bus("ABC124", 2012, 9.7, 25)
        );

        mySite.registerVehicle(
                new Truck("ABC125", 2014, 12.7, 2.6)
        );

        list(mySite);
    }

    private static void list(Site site) {
        for(Vehicle v: site.getVehicles()) {
            System.out.println(v);
        }
    }
}
