package Episode04_05_06_hu;

public interface IPassengerTransporter {
    int getNumberOfPassengers();
}
