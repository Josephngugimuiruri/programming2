package Episode04_05_06_hu;

public class Elevator implements IPassengerTransporter, ICargo {
    private int numberOfPersons;
    private double maxWeight;

    public Elevator(int numberOfPersons, double maxWeight) {
        this.numberOfPersons = numberOfPersons;
        this.maxWeight = maxWeight;
    }

    @Override
    public int getNumberOfPassengers() {
        return numberOfPersons;
    }

    @Override
    public double getMaxWeight() {
        return maxWeight;
    }
}
