package Episode04_05_06_hu;

public abstract class Vehicle {
    private String licence;
    private int prodYear;
    private double consumption;
    private int kmRun;

    public String getLicence() {
        return licence;
    }

    public int getProdYear() {
        return prodYear;
    }

    public double getConsumption() {
        return consumption;
    }

    public int getKmRun() {
        return kmRun;
    }

    public Vehicle(String licence, int prodYear, double consumption) {
        this.licence = licence;
        this.prodYear = prodYear;
        this.consumption = consumption;
        kmRun = 0;
    }

    public void addKm(int additionalKm) {
        if(additionalKm >= 0) {
            kmRun += additionalKm;
        }
    }

    @Override
    public String toString() {
        return String.format("%s (%d)", licence, prodYear);
    }
}
