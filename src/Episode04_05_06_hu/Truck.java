package Episode04_05_06_hu;

public class Truck extends Vehicle implements ICargo {
    private static int baseRentalFee;

    public static int getBaseRentalFee() {
        return baseRentalFee;
    }

    public static void setBaseRentalFee(int baseRentalFee) {
        Truck.baseRentalFee = baseRentalFee;
    }

    private double maxWeight;

    public double getMaxWeight() {
        return maxWeight;
    }

    public Truck(
            String licence, int prodYear, double consumption,
            double maxWeight
    ) {
        super(licence, prodYear, consumption);
        this.maxWeight = maxWeight;
    }

    @Override
    public String toString() {
        return String.format("Teherautó %s súly: %d",
                super.toString(), getMaxWeight()
        );
    }
}
