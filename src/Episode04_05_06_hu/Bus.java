package Episode04_05_06_hu;

public class Bus extends PassengerTransporter {
    private static int baseRentalFee;

    public static int getBaseRentalFee() {
        return baseRentalFee;
    }

    public static void setBaseRentalFee(int baseRentalFee) {
        Bus.baseRentalFee = baseRentalFee;
    }

    public Bus(String licence, int prodYear, double consumption, int passengers) {
        super(licence, prodYear, consumption, passengers);
    }

    @Override
    public String toString() {
        return String.format("Bus %s [%d]",
                super.toString(), getNumberOfPassengers()
        );
    }
}
