package Episode04_05_06_hu;

import java.util.ArrayList;

public class Site {
    private ArrayList<Vehicle> vehicles;
    private String name;
    private String address;
    private String id;
    private int maxVehicleCount;

    public ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    public int getMaxVehicleCount() {
        return maxVehicleCount;
    }

    public Site(String name, String address, String id, int maxVehicleCount) {
        this.name = name;
        this.address = address;
        this.id = id;
        this.maxVehicleCount = maxVehicleCount;
        this.vehicles = new ArrayList<>();
    }

    public void registerVehicle(Vehicle newVehicle) {
        if(vehicles.size() < maxVehicleCount) {
            vehicles.add(newVehicle);
        }
    }

    public Vehicle find(String licence) {
        for(Vehicle v: vehicles) {
            if(v.getLicence().equalsIgnoreCase(licence)) {
                return v;
            }
        }
        return null;
    }

    public Bus find(int numberOfPersons) {
        for(Vehicle v: vehicles) {
            if(
                    v instanceof Bus &&
                    ((Bus)v).getNumberOfPassengers() >= numberOfPersons
            ) {
                return (Bus)v;
            }
        }
        return null;
    }

    public Car find(int numberOfPersons, int comfortLevel) {
        for(Vehicle v: vehicles) {
            if(
                    v instanceof Car
                    && ((Car)v).getNumberOfPassengers() >= numberOfPersons
                    && ((Car)v).getComfortLevel() >= comfortLevel
            ) {
                return (Car)v;
            }
        }
        return null;
    }
}






