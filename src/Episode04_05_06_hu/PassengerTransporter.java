package Episode04_05_06_hu;

public abstract class PassengerTransporter
        extends Vehicle
        implements IPassengerTransporter
{

    private int numberOfPassengers;

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public PassengerTransporter(
            String licence, int prodYear, double consumption,
            int passengers
    ) {
        super(licence, prodYear, consumption);
        this.numberOfPassengers = passengers;
    }
}
