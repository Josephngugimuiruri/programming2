package Episode03_hu;

public class ContestantIdGenerator {
    private static int lastParticipantId = 0;

    public static int getNextId() {
        return ++lastParticipantId;
    }
}
