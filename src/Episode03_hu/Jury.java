package Episode03_hu;

import java.util.Random;

public class Jury {
    private static int maxPoints = 10;
    private static Random rnd = new Random();

    public static void setMaxPoints(int maxPoints) {
        Jury.maxPoints = maxPoints;
    }

    private String name;

    public String getName() {
        return name;
    }

    public Jury(String name) {
        this.name = name;
    }

    public int givePoints() {
        return rnd.nextInt(maxPoints) + 1;
    }
}
