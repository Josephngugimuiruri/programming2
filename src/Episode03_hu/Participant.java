package Episode03_hu;

public class Participant {
    private int id;
    private String name;
    private int age;
    private String faculty;
    private int point;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getFaculty() {
        return faculty;
    }

    public int getPoint() {
        return point;
    }

    public Participant(String name, int age, String faculty) {
        this.id = ContestantIdGenerator.getNextId();
        this.name = name;
        this.age = age;
        this.faculty = faculty;
        this.point = 0;
    }

    public void addPoints(int newPoints) {
        point += newPoints;
    }
}
