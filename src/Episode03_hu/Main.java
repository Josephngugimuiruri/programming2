package Episode03_hu;

import java.util.Scanner;

public class Main {
    public static void main() {
        Scanner consoleScanner = new Scanner(System.in);

        Contest contest = new Contest();
        contest.registerJury(consoleScanner);
        contest.registerParticipants(consoleScanner);
        contest.listJury();
        contest.listParticipants();
        contest.evaluateParticipants();
        contest.listParticipants();
        contest.sortParticipants();
        contest.listParticipants();
    }
}
