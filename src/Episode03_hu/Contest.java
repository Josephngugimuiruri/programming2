package Episode03_hu;

import java.util.Scanner;

public class Contest {
    private Jury[] juries;
    private Participant[] participants;

    public void registerJury(Scanner scanner) {
        System.out.print("Zsűritagok száma: ");
        int juryCount = scanner.nextInt();
        juries = new Jury[juryCount];

        String name;
        for (int i=0; i<juries.length; i++) {
            System.out.format("Kérem adja meg a %d. zsűritag nevét:", i+1);
            name = scanner.nextLine();
            juries[i]=new Jury(name);
        }
    }

    public void registerParticipants(Scanner scanner) {
        System.out.print("Résztvevők száma: ");
        int participantCount = scanner.nextInt();
        participants = new Participant[participantCount];

        String name;
        int age;
        String faculty;
        for (int i=0; i<participants.length; i++) {
            System.out.format("Kérem adja meg a %d. résztvevő nevét:", i+1);
            name = scanner.nextLine();
            System.out.format("Kérem adja meg a %d. résztvevő korát:", i+1);
            age = scanner.nextInt();
            System.out.format("Kérem adja meg a %d. résztvevő szakját:", i+1);
            faculty = scanner.nextLine();
            participants[i]=new Participant(name, age, faculty);
        }
    }

    public void listJury() {
        for(Jury jury: juries) {
            System.out.format("Zsűritag: %s\n", jury.getName());
        }
    }

    public void listParticipants() {
        for(Participant participant: participants) {
            System.out.format(
                    "Versenyző: %s (#%d) pontjai: %d\n",
                    participant.getName(),
                    participant.getId(),
                    participant.getPoint()
            );
        }
    }

    public void evaluateParticipants() {
        for(Participant participant: participants) {
            for(Jury jury: juries) {
                participant.addPoints(
                        jury.givePoints()
                );
            }
        }
    }

    public void sortParticipants() {
        boolean hasChange;
        Participant temp;
        do {
            hasChange = false;
            for (int i = 0; i < participants.length - 1; i++) {
                if(participants[i].getPoint() < participants[i+1].getPoint()){
                    temp = participants[i];
                    participants[i] = participants[i+1];
                    participants[i+1] = temp;
                    hasChange = true;
                }
            }
        }while(hasChange);
    }
}
