package Episode03_en;

import java.util.Scanner;

public class Show {
    private String name;
    private Participant[] participants;
    private Jury[] juries;

    public String getName() {
        return name;
    }

    public Show(String name) {
        this.name = name;
    }

    public void printParticipants() {
        for (Participant participant: participants) {
            System.out.format(
                    "name: %s (#%d) points: %d\n",
                    participant.getName(),
                    participant.getId(),
                    participant.getPoints()
            );
        }
    }

    public void printJury() {
        for(Jury jury: juries) {
            System.out.format(
              "Name: %s\n",
              jury.getName()
            );
        }
    }

    public void sortParticipants() {
        boolean hasChange;
        do {
            hasChange = false;
            for (int i = 0; i < participants.length - 1; i++) {
                if (participants[i].getPoints() < participants[i + 1].getPoints()) {
                    Participant temp;
                    temp = participants[i];
                    participants[i] = participants[i + 1];
                    participants[i + 1] = temp;
                    hasChange = true;
                }
            }
        }while(hasChange);
    }

    public void start(
            int numerOfJuries,
            int numberOfPraticipants,
            Scanner scanner
    ) {
        recruitJury(numerOfJuries, scanner);
        recruitParticipants(numberOfPraticipants, scanner);

        printJury();
        System.out.println("Initial ranking");
        printParticipants();

        evaluateParticipants();

        sortParticipants();
        System.out.println("Final result");
        printParticipants();
    }

    private void recruitJury(int numerOfJuries, Scanner scanner) {
        juries = new Jury[numerOfJuries];
        for (int i = 0; i < numerOfJuries; i++) {
            System.out.format("Enter %d. jury name: ", i+1);
            juries[i] = new Jury(scanner.next());
        }
    }

    private void recruitParticipants(int numberOfPraticipants, Scanner scanner) {
        participants = new Participant[numberOfPraticipants];
        for (int i = 0; i < participants.length; i++) {
            System.out.format("Enter %d. participant name: ", i+1);
            String name = scanner.next();

            System.out.format("Enter %d. participant age: ", i+1);
            int age = scanner.nextInt();

            participants[i] = ParticipantFactory.createParticipant(
                    name,
                    age
            );
        }
    }

    private void evaluateParticipants() {
        int points;
        for(Participant participant: participants){
            System.out.format("Evaluating %s\n", participant.getName());
            for(Jury jury: juries) {
                points = jury.givePoint();
                System.out.format("\tPoints from %s: %d\n",
                        participant.getName(),
                        points
                );
                participant.addPoints(points);
            }
        }
    }
}
