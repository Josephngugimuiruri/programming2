package Episode03_en;

import java.util.Scanner;

public class Main {

    public static void main(int number) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter title of show: ");
        Show show = new Show(scanner.nextLine());

        System.out.print("Enter number of jury: ");
        int juryCount = scanner.nextInt();
        System.out.print("Enter number of participants: ");
        int participantCount = scanner.nextInt();


        System.out.print("Maximum of points: ");
        Jury.setMaxPoint(scanner.nextInt());

        show.start(juryCount, participantCount, scanner);
    }
}
