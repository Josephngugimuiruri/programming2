package Episode03_en;

public class Participant {
    private String name;
    private int age;
    private int points;
    private int id;

    public Participant(String name, int age, int id) {
        this.name = name;
        this.age = age;
        this.id = id;
        this.points = 0;
    }

    public int getPoints() {
        return points;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    public void addPoints(int additionalPoints) {
        this.points += additionalPoints;
    }
}
