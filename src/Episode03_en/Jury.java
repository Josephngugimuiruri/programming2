package Episode03_en;

import java.util.Random;

public class Jury {
    private static int maxPoint = 10;

    public static void setMaxPoint(int maxPoint) {
        Jury.maxPoint = maxPoint;
    }

    private String name;
    private static Random rnd = new Random();

    public String getName() {
        return name;
    }

    public Jury(String name) {
        this.name = name;
    }

    public int givePoint() {
        return rnd.nextInt(maxPoint) + 1;
    }
}
