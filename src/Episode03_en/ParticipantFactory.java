package Episode03_en;

public class ParticipantFactory {
    private static int numberOfParticipantsCreated = 0;

    public static Participant createParticipant(
            String name,
            int age
    ) {
        return new Participant(name, age, ++numberOfParticipantsCreated);
    }
}
